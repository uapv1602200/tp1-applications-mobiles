package com.example.tp1;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import 	android.widget.AdapterView;
import android.widget.ArrayAdapter;
import       android.widget.ListView;
import 	android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Toolbar toolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        final ListView listview = (ListView) findViewById(R.id.list_view);
        String[] values = new String[]{"Renard roux", "Koala", "Windows10", "Montbéliarde", "Panda géant", "Ours brun","Chameau", "Lion" };

        // Create an ArrayAdapter, that will actually make the Strings above appear in the ListView
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                // Do something in response to the click
                final String item = (String) parent.getItemAtPosition(position);
                Toast.makeText(MainActivity.this, "You selected: " + item, Toast.LENGTH_LONG).show();
            }
        });

        /*FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }

}